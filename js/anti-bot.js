document.addEventListener('DOMContentLoaded', function() {
    // Initialize an agent at application startup, once per page/app.
    const botdPromise = import('https://openfpcdn.io/botd/v1').then((Botd) => Botd.load())

        // Get detection results when you need them.
        botdPromise
            .then((botd) => botd.detect())
            .then((response) => {
                // Check if the response indicates a bot
                const isBot = response.bot === true;

                // Check if the result indicates a bot
                if (isBot) {
                    // Display an error message for bots
                    alert('Forbidden: You are not allowed to access this page.');
                    window.location.replace('403.html');
                } else {
                    // Redirect to the home page for non-bots
                    console.log('Verification successful.')
                    return;
                }

                // Log the result
                console.log('Is bot:', isBot);
            })
            .catch((error) => console.error(error))
    });
